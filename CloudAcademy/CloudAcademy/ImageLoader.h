#pragma once
#include "GlTexture.h"
#include"PicoPNG.h"
#include"IOManager.h"
#include"Errors.h"
#include<string>

class ImageLoader
{
public:
	static GlTexture loadPNG(std::string filePath);
};

