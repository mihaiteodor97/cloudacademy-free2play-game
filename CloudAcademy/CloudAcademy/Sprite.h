#pragma once
#include"Vertex.h"
#include "ResourceManager.h"
#include<GL/glew.h>
#include<cstddef>
#include<string>

class Sprite
{
public:
	Sprite();
	~Sprite();

public:
	void draw();
	void init(float x, float y, float width, float height, std::string texturePath);

private:
	float _x, _y, _width, _height;

private:
	GLuint _vboID;
	GlTexture _texture;
};

