#include "ResourceManager.h"

TextureCache ResourceManager::_textureCache;
GLuint ResourceManager::boundTexture;

GlTexture ResourceManager::getTexture(std::string texturePath)
{
	return _textureCache.getTexture(texturePath);
}
