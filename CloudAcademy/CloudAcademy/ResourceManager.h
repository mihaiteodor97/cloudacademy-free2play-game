#pragma once
#include"TextureCache.h"

class ResourceManager
{
public:
	static GlTexture getTexture(std::string texturePath);

private:
	static TextureCache _textureCache;
	static GLuint boundTexture;
};

