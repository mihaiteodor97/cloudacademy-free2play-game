#pragma once
#include"Sprite.h"
#include"GLSLProgram.h"
#include<Windows.h>
#include<SDL/SDL.h>
#include<GL/glew.h>
#include"GlTexture.h"

enum class GameState { PLAY, EXIT };

class MainGame
{
public:
	MainGame();
	~MainGame();

public:
	void run();

private:
	void initSystems();
	void initShaders();
	void processInput();
	void gameLoop();
	void drawGame();
	void calculateFPS();

private:
	int _screenWidth, _screenHeight;
	float _time, _fps, _frameTime, _maxFPS;

private:
	SDL_Window* _window;
	GameState _gameState;
	GLSLProgram _colorProgram;
	std::vector<Sprite*> _sprites;
	//Sprite _testSprite;
};

