#pragma once
#include<map>
#include "GlTexture.h"
#include<string>

class TextureCache
{
public:
	TextureCache();
	~TextureCache();

public:
	GlTexture getTexture(std::string filePath);

private:
	std::map<std::string, GlTexture> _textureMap;
};

