#include "TextureCache.h"
#include"ImageLoader.h"

TextureCache::TextureCache()
{
}

TextureCache::~TextureCache()
{
}

GlTexture TextureCache::getTexture(std::string filePath)
{
	//std::map<std::string, GlTexture>::iterator mapIt = 
	auto mapIt = _textureMap.find(filePath);

	//check if texture not in map, load it
	if (mapIt == _textureMap.end())
	{
		GlTexture newTexture = ImageLoader::loadPNG(filePath);

		_textureMap.insert(make_pair(filePath, newTexture));

		std::cout << "Loaded Texture\n";
		return newTexture;
	}

	std::cout << "Used Cached Texture\n";
	return mapIt->second;
}
