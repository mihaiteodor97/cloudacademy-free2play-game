#include "MainGame.h"
#include"Errors.h"
#include<iostream>
#include<string>


MainGame::MainGame():
	_window(nullptr), _screenWidth(1024),
	_screenHeight(768), _gameState(GameState::PLAY), 
	_time(0.0f), _maxFPS(60.0f)
{

}

MainGame::~MainGame()
{

}

void MainGame::run()
{
	initSystems();
	
	//hardcoded for test
	_sprites.push_back(new Sprite());
	_sprites.back()->init(-1.0f, -1.0f, 1.0f, 1.0f, "Textures/jimmyJumpPack/PNG/CharacterRight_Standing.png");
	
	_sprites.push_back(new Sprite());
	_sprites.back()->init(0.0f, -1.0f, 1.0f, 1.0f, "Textures/jimmyJumpPack/PNG/CharacterRight_Standing.png");

	gameLoop();
}

void MainGame::initSystems()
{
	SDL_Init(SDL_INIT_EVERYTHING);

	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

	_window = SDL_CreateWindow("Cloud Academy", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, _screenWidth, _screenHeight, SDL_WINDOW_OPENGL);
	if (_window == nullptr)
	{
		fatalError("SDL Window could not be created!");
	}

	SDL_GLContext glContext = SDL_GL_CreateContext(_window);
	if (glContext == nullptr) {
		fatalError("SDL_GL context could not be created!");
	}

	GLenum error = glewInit();
	if (error != GLEW_OK) {
		fatalError("Could not initialize glew!");
	}

	//Check the OpenGL Version
	printf("***   OpenGL Version: %s   ***", glGetString(GL_VERSION));
	std::cout << std::endl;

	glClearColor(0.0f, 0.0f, 0.0f, 1.0);

	//1 = VSync on
	SDL_GL_SetSwapInterval(0);

	initShaders();
}

void MainGame::initShaders()
{
	_colorProgram.compileShaders("Shaders/colorShading.vert", "Shaders/colorShading.frag");
	_colorProgram.addAttribute("vertexPosition");
	_colorProgram.addAttribute("vertexColor");
	_colorProgram.addAttribute("vertexUV");
	_colorProgram.linkShaders();
}

void MainGame::processInput()
{
	SDL_Event evnt;
	while (SDL_PollEvent(&evnt)) {
		switch (evnt.type) {
		case SDL_QUIT:
			_gameState = GameState::EXIT;
			break;
		case SDL_MOUSEMOTION:
			//std::cout << evnt.motion.x << " " << evnt.motion.y << std::endl;
			break;
		}
	}
}

void MainGame::gameLoop()
{
	while (_gameState != GameState::EXIT) {
		//used for frametime measuring
		float startTicks = SDL_GetTicks();

		processInput();
		_time += 0.001;
		drawGame();
		calculateFPS();

		//print only once every 10 frames
		static int frameCounter = 0;
		frameCounter++;
		if (frameCounter == 30) {
			std::cout << _fps << std::endl;
			frameCounter = 0;
		}

		float frameTicks = SDL_GetTicks() - startTicks;
		//Limit the fps to the max fps
		if (1000.0f / _maxFPS > frameTicks) {
			SDL_Delay(1000.0f / _maxFPS - frameTicks);
		}

	}
}

void MainGame::drawGame()
{
	glClearDepth(1.0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);	//will clear both

	_colorProgram.use();
	glActiveTexture(GL_TEXTURE0);	//use the 1st texture, not multitexturing!

	//see ColorShading.frag in shaders
	GLint textureLocation = _colorProgram.getUniformLocation("mySampler");
	glUniform1i(textureLocation, 0);	//bind texture to texture0

	GLint timeLocation = _colorProgram.getUniformLocation("time");
	glUniform1f(timeLocation, _time);

	//hardcoded for test
	for (int i = 0; i < _sprites.size(); ++i) {
		_sprites[i]->draw();
	}

	glBindTexture(GL_TEXTURE_2D, 0);	//unbind texture

	_colorProgram.unuse();

	SDL_GL_SwapWindow(_window);
}

void MainGame::calculateFPS()
{
	static const int NUM_SAMPLES = 10;
	static float frameTimes[NUM_SAMPLES];
	static int currentFrame = 0;
	static float previousTicks = SDL_GetTicks();
	float currentTicks;
	int count;
	float frameTimeAverage = 0;

	currentTicks = SDL_GetTicks();

	_frameTime = currentTicks - previousTicks;

	previousTicks = currentTicks;

	frameTimes[currentFrame % NUM_SAMPLES] = _frameTime;

	currentFrame++;
	if (currentFrame < NUM_SAMPLES) {
		count = currentFrame;
	}
	else {
		count = NUM_SAMPLES;
	}

	for (int i = 0; i < count; ++i) {
		frameTimeAverage += frameTimes[i];
	}

	frameTimeAverage /= count;

	if (frameTimeAverage > 0) {
		_fps = 1000.0f / frameTimeAverage;
	}
	else {
		_fps = 60.0f;	//so that it has a value this frame
	}
}
