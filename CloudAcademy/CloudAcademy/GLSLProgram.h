#pragma once
#include"Errors.h"
#include<string>
#include<fstream>
#include<vector>
#include<GL/glew.h>

class GLSLProgram
{
public:
	GLSLProgram();
	~GLSLProgram();

public:
	void compileShaders(const std::string& vertexShaderFilePath, const std::string& fragmentShaderFilePath);
	void linkShaders();
	void addAttribute(const std::string& attributeName);
	void use();
	void unuse();
	GLuint getUniformLocation(const std::string& uniformName);

private:
	void compileShader(const std::string filePath, GLuint& id);

private:
	GLuint _programID, _vertexShaderID, _fragmentShaderID;
	int _numAttributes;
};

